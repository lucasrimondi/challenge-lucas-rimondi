import './styles.css';

const userUrl = "https://www.mockachino.com/a71b232c-218e-4d/users";
let users = [];
let paginatorObject;
let filteredUsers;

//HTML REFS
const usersList       = document.getElementById('usersList');
const searchBar       = document.getElementById('searchBar');
const selectAtoZ      = document.getElementById('selectAtoZ');
const optionToDisable = document.getElementById('optionToDisable');
const prevBtn         = document.getElementById('prevBtn');
const nextBtn         = document.getElementById('nextBtn');
const headerTitle     = document.getElementById('title');

//LOAD USERS FROM API
const loadUsers = async() => {
    try {
        const resp = await fetch(userUrl);
        const data = await resp.json(); 
        users = data.results;
        displayUsers(paginator(users, paginatorObject.page, paginatorObject.per_page).data);
        filteredUsers = users;
    } catch (err) {
        console.error(err);
    }
};

//RENDER USERS FUNCTION
const displayUsers = (usersToDisplay) => {

    const htmlString = usersToDisplay
        .map((user) => {
            return `
            <div class="card" id="divCard">
                <img class= "cardImg" src="${user.picture.large}">     
                <div class = "data"> 
                    <h2 class = "name">${user.name.first} ${user.name.last}</h1>
                    <h2 class = "location">&#8982 ${user.location.city}, ${user.location.country}</h2>
                    <h2 class = "mail">&#9993 ${user.email}</h2>
                </div>  
            </div>  
                   `;
            }).join('');

    usersList.innerHTML = htmlString;

    if (paginatorObject.page == paginatorObject.total_pages && paginatorObject.page === 1  || paginatorObject.total_pages == 0 ){
        prevBtn.hidden = true;
        nextBtn.hidden = true;
    } else if (paginatorObject.page == paginatorObject.total_pages) {
        nextBtn.hidden = true;
        prevBtn.hidden = false;
    } else if (paginatorObject.page === 1){
        prevBtn.hidden = true;
        nextBtn.hidden = false; 
    } else {
        prevBtn.hidden = false;
        nextBtn.hidden = false;    
    };

    headerTitle.innerHTML = 
        paginatorObject.total_pages == 0 ?
        `No users found`
        :
        `Showing Page ${paginatorObject.page}-${paginatorObject.total_pages}  of&nbsp<font color= "#2491ff">  ${paginatorObject.total} users </font>`
};

//SEARCHBAR FILTER
searchBar.onkeyup = (e) => {

    paginatorObject.page = 1;

    let searchString = e.target.value.toLowerCase();

    filteredUsers = users.filter((user) => {
        return (
            user.name.first.toLowerCase().includes(searchString) 
            ||
            user.name.last.toLowerCase().includes(searchString)
        );
    });

    if (searchString == '') {
        filteredUsers = users
    };

    displayUsers(paginator(filteredUsers, paginatorObject.page, paginatorObject.per_page).data);

    nextBtn.onclick = () => {
        displayUsers(paginator(filteredUsers, paginatorObject.next_page, paginatorObject.per_page).data)
    };
    prevBtn.onclick = () => {
        displayUsers(paginator(filteredUsers, paginatorObject.pre_page, paginatorObject.per_page).data)
    };
};

//A TO Z FILTER
selectAtoZ.onchange = (e) => {
    let selection = e.target.value;

    optionToDisable.disabled = true;
    nextBtn.hidden           = false;  
    // searchBar.value          = '';

    paginatorObject.page = 1;

    console.log(filteredUsers)

    let orderedUsers = (selection == 'az') ? 
        
        filteredUsers.sort((a, b) => {
            if(a.name.first < b.name.first)  return -1 })
        : 
        filteredUsers.sort((a, b) => {
            if(b.name.first < a.name.first)  return -1 })

    filteredUsers = orderedUsers;   

    displayUsers(paginator(filteredUsers, paginatorObject.page, paginatorObject.per_page).data);
};

//PAGINATOR
const paginator = (items, current_page = 1, per_page_items) => {
    let page = current_page,
        per_page = per_page_items || 10,
        offset = (page - 1) * per_page,

        paginatedItems = items.slice(offset).slice(0, per_page_items),
        total_pages = Math.ceil(items.length / per_page);

    return paginatorObject = {
        page: page,
        per_page: per_page,
        pre_page: page - 1 ? page - 1 : null,
        next_page: (total_pages > page ) ? page + 1 : null,
        total: items.length,
        total_pages: total_pages,
        data: paginatedItems
    };
};

paginator(users);

nextBtn.onclick = () => {
    displayUsers(paginator(filteredUsers, paginatorObject.next_page, paginatorObject.per_page).data)
};

prevBtn.onclick = () => {
    displayUsers(paginator(filteredUsers, paginatorObject.pre_page, paginatorObject.per_page).data)
};

loadUsers();


